#ifndef PARSER_H
#define PARSER_H

#include "mainwindow.h"
#include "city.h"


class Parser
{
public:
    Parser();
    QJsonDocument getJsonWeatherData(QString city_name);
//    City getCityObject(QString city_name);
};
#endif // PARSER_H
