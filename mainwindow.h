#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QApplication>
#include <QtGui>
#include <QSplashScreen>

#include <QDataStream>
#include <QtSql>
#include <QJsonDocument>

#include <QMainWindow>
#include <QUrl>
#include <QDateTime>
#include <QtXml>
#include <QXmlStreamAttributes>
#include <QTimer>
#include <QtNetwork/QNetworkRequest>

#include <QNetworkAccessManager>
#include <QObject>
#include <QNetworkReply>
#include <QXmlStreamReader>
#include <QTimeZone>

#include "mainwindow.h"
#include "parser.h"
#include "city.h"
#include "database.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
     void doDownload(const QString city);
public slots:
    void replyFinished (QNetworkReply *reply);

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_6_clicked();

    void showTime();
    void date3();

    void parserXML(QString city);
    QString weather_symbol(QString symbol);
    QString weather_description(QString symbol);
    QString bgCity(QString cityName);


    void on_settings_clicked();


    void on_moreCity_activated(const QString &arg1);
    void lengthString(QString city);
    QString setCity(QString rusCity);

private:
    Ui::MainWindow *ui;
    QNetworkAccessManager *manager;
};

#endif // MAINWINDOW_H
