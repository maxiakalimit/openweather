#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //Дни недели
    QDate day_week = QDate::currentDate();
    ui->weather_hours->setText(day_week.addDays(+1).toString("ddd"));
    ui->weather_hours_2->setText(day_week.addDays(+2).toString("ddd"));
    ui->weather_hours_3->setText(day_week.addDays(+3).toString("ddd"));


    //Первый запрос к API
//    doDownload("moscow");
    parserXML("moscow");

    //Запрос к API каждые 30 мин
    QTimer *api_timer = new QTimer(this);
    connect(api_timer,SIGNAL(timeout()), this, SLOT(parserXML()));
    api_timer->start(1800000);

    //Скачивание каждые 30 мин
    QTimer *file_timer = new QTimer(this);
    connect(file_timer,SIGNAL(timeout()), this, SLOT(doDownload()));
    file_timer->start(1800000);

    //Обновление времени
    QTimer *date_timer = new QTimer(this);
    connect(date_timer,SIGNAL(timeout()), this, SLOT(showTime()));
    date_timer->start();

    //Дни недели(погода). Обновление каждые 30 мин
    QTimer *date_4day = new QTimer(this);
    connect(date_4day,SIGNAL(timeout()), this, SLOT(date3()));
    date_4day->start(1800000);

    //Подсказки
    ui->pushButton->setToolTip("Москва");
    ui->pushButton_2->setToolTip("Санкт-Петербург");
    ui->pushButton_3->setToolTip("Новосибирск");
    ui->pushButton_4->setToolTip("Казань");
    ui->pushButton_5->setToolTip("Чита");
    ui->pushButton_6->setToolTip("Нижний Новгород");

    //СТИЛИ
    //Другие Города
    ui->moreCity->setStyleSheet("QComboBox#moreCity{"
                                    "border: 4px solid #333333;"
                                    "border-radius: 0px;"
                                    "background: #fff;"
                                    "padding: 1px 23px 1px 3px;"
                                    "selection-background-color: red;"
                                    "font-size:12pt;"
                                    "color: #000;}");
}


QString globalCity;
void MainWindow::doDownload(const QString city)
{
    manager = new QNetworkAccessManager(this);

    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(replyFinished(QNetworkReply*)));

    QString strUrl = "http://api.openweathermap.org/data/2.5/forecast/daily?q=" + city + "&mode=xml&units=metric&cnt=7&APPID=ce263317c3bdb57a034cffbbde28b20f";
    //manager->get(QNetworkRequest(QUrl("http://api.openweathermap.org/data/2.5/forecast/daily?q=moscow&mode=xml&units=metric&cnt=7&APPID=ce263317c3bdb57a034cffbbde28b20f")));
    manager->get(QNetworkRequest(QUrl(strUrl)));
    globalCity = city;
}
void MainWindow::replyFinished(QNetworkReply *reply)
{
    if(reply->error())
    {
        qDebug() << "ERROR!";
        qDebug() << reply->errorString();
    }
    else
    {
        QFile *file = new QFile("/Users/spase/qt/global_weather/" + globalCity + ".xml"); //Добавить создание папки на пк пользователя
        if(file->open(QFile::Append))
        {
            file->seek(0);
            file->write(reply->readAll());
            file->flush();
            file->close();
        }
        delete file;
    }

    reply->deleteLater();
}


void MainWindow::parserXML(QString city)
{

    City city_obj = City(city);


    QString styleSheet0 = "QWidget#all_widget {background-image: url(%1); background-position:left center; background-repeat:no-repeat;}";
    ui->all_widget->setStyleSheet(styleSheet0.arg(bgCity(city_obj.name)));


     //Средняя температура
     ui->weather_cel->setText(QString("%1°").arg(   floor(city_obj.day_temp[0].toFloat())  ));
     ui->weather_cel_2->setText(QString("%1°").arg(floor(city_obj.day_temp[1].toFloat())));
     ui->weather_cel_3->setText(QString("%1°").arg(floor(city_obj.day_temp[2].toFloat())));
     ui->weather_cel_4->setText(QString("%1°").arg(floor(city_obj.day_temp[3].toFloat())));

        //Макс. темп.
     ui->w_max->setText(QString("%1°").arg( floor(city_obj.temp_max[0].toFloat() )));

        //Мин. темп.
      ui->w_min->setText(QString("%1°").arg( floor(city_obj.temp_min[0].toFloat()) ));

        //Иконки
      QString styleSheet = "QWidget#ico_now {background-image: url(%1); background-repeat:no-repeat}";
      QString styleSheet1 = "QWidget#ico_now_2 {background-image: url(%1); background-repeat:no-repeat}";
      QString styleSheet2 = "QWidget#ico_now_3 {background-image: url(%1); background-repeat:no-repeat}";
      QString styleSheet3 = "QWidget#ico_now_4 {background-image: url(%1); background-repeat:no-repeat}";

      ui->ico_now->setStyleSheet(styleSheet.arg(weather_symbol(city_obj.weather_description[0])));
      ui->ico_now_2->setStyleSheet(styleSheet1.arg(weather_symbol(city_obj.weather_description[1])));
      ui->ico_now_3->setStyleSheet(styleSheet2.arg(weather_symbol(city_obj.weather_description[2])));
      ui->ico_now_4->setStyleSheet(styleSheet3.arg(weather_symbol(city_obj.weather_description[3])));

      //Описание погоды
      ui->text_now->setText(weather_description(city_obj.weather_description[0] ));
}

void MainWindow::date3()
{
    QDate day_week = QDate::currentDate();
    ui->weather_hours->setText(day_week.addDays(+0).toString("ddd"));
    ui->weather_hours_2->setText(day_week.addDays(+1).toString("ddd"));
    ui->weather_hours_3->setText(day_week.addDays(+2).toString("ddd"));
}
//Время и дата
void MainWindow::showTime()
{
    //DateTime
    QDateTime date = QDateTime::currentDateTime();
    QDateTime time = QDateTime::currentDateTime();

    ui->date_now->setText(date.toString("Сегодня dddd"));
    ui->time_now->setText(time.toString("hh:mm:ss"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

//События Кнопки
void MainWindow::on_pushButton_clicked()
{
    parserXML("moscow");
    ui->city->setStyleSheet("color:#fff; background-color:rgba(0,0,0,0)");
    ui->city->setText("Москва");
    ui->weather_cel->setStyleSheet("color:#fff; background-color:rgba(0,0,0,0)");

    QString date = QDateTime::currentDateTime().toString("dd.MM.yy");
    QString time = QDateTime::currentDateTime().toString("hh:mm");
    ui->date_now->setText(date);
    ui->time_now->setText(time);
}

void MainWindow::on_pushButton_2_clicked()
{
    parserXML("saintpetersburg");
    ui->city->setStyleSheet("color:#fff; background-color:rgba(0,0,0,0); font-size:45pt;");
    ui->city->setText("Санкт-Петербург");

}

void MainWindow::on_pushButton_3_clicked()
{
    parserXML("novosibirsk");
    ui->city->setStyleSheet("color:#fff; background-color:rgba(0,0,0,0)");
    ui->city->setText("Новосибирск");

}

void MainWindow::on_pushButton_4_clicked()
{
     parserXML("kazan");

     ui->city->setText("Казань");
}
void MainWindow::on_pushButton_5_clicked()
{
     parserXML("chita");

     ui->city->setStyleSheet("color:#fff; "
                             "background-color:rgba(0,0,0,0)");
     ui->city->setText("Чита");
}

void MainWindow::on_pushButton_6_clicked()
{
    parserXML("nizhnynovgorod");

    ui->city->setStyleSheet("color:#fff; "
                            "background-color:rgba(0,0,0,0); "
                            "font-size:45pt;");
    ui->city->setText("Нижний Новгород");
}


void MainWindow::on_settings_clicked()
{

}

//background для Городов
QString MainWindow::bgCity(QString cityName)
{
    QString link = ":assets/img/city/%1.jpg";
    return(link.arg(cityName));
}


void MainWindow::on_moreCity_activated(const QString &arg1)
{
         QString enCity = setCity(arg1);
         doDownload(enCity);
         parserXML(enCity);
         lengthString(arg1);
}

//Длина строки и вывод в область City
void MainWindow::lengthString(QString city)
{
    if (city.length() > 11){
        ui->city->setStyleSheet("color:#fff; "
                                "background-color:rgba(0,0,0,0); "
                                "font-size:45pt;");
        ui->city->setText(city);
    }
    else{
        ui->city->setStyleSheet("color:#fff; "
                                "background-color:rgba(0,0,0,0); "
                                "font-size:58pt;");
        ui->city->setText(city);
    }
}

QString MainWindow::weather_symbol(QString symbol)
{
    if (symbol == "snow")
    {
        return(":/symbol/img/sympol/snow_min.png");
    }
    else if (symbol == "sky is clear")
    {
        return(":/symbol/img/sympol/sky_is_clear_min.png");
    }
    else if (symbol == "few clouds")
    {
        return(":/symbol/img/sympol/few_clouds_min.png");
    }
    else if (symbol == "scattered clouds")
    {
        return(":/symbol/img/sympol/scattered_clouds_min.png");
    }
    else if (symbol == "broken clouds")
    {
        return(":/symbol/img/sympol/broken_clouds_min.png");
    }
    else if (symbol == "shower rain")
    {
        return(":/symbol/img/sympol/shower_rain_min.png");
    }
    else if (symbol == "Rain")
    {
        return(":/symbol/img/sympol/rain_min.png");
    }
    else if (symbol == "light rain")
    {
        return(":/symbol/img/sympol/rain_min.png");
    }
    else if (symbol == "moderate rain")
    {
        return(":/symbol/img/sympol/rain_min.png");
    }
    else{
        return(":/symbol/img/sympol/snow_min.png");
    }
}

QString MainWindow::weather_description(QString symbol)
{
    if (symbol == "snow")
    {
        return("Снег");
    }
    else if (symbol == "sky is clear")
    {
        return("Ясно");
    }
    else if (symbol == "few clouds")
    {
        return("Переменная облачность");
    }
    else if (symbol == "scattered clouds")
    {
        return("Облачно");
    }
    else if (symbol == "broken clouds")
    {
        return("Пасмурно");
    }
    else if (symbol == "Rain")
    {
        return("Дождь");
    }
    else if (symbol == "light rain")
    {
        return("Небольшой дождь");
    }
    else if (symbol == "moderate rain")
    {
        return("Небольшой дождь");
    }
    else if (symbol == "light snow")
    {
        return("Небольшой снег");
    }
    else{
        return("Что-то пошло не так");
    }
}

QString MainWindow::setCity(QString rusCity)
{
    if (rusCity == "Москва")
    {
        return("moscow");
    }
    if (rusCity == "Уфа")
    {
        return("ufa");
    }
    if (rusCity == "Санкт-Петербург")
    {
        return("saintpetersburg");
    }
    if (rusCity == "Чита")
    {
        return("chita");
    }
    if (rusCity == "Челябинск")
    {
        return("chelyabinsk");
    }
    if (rusCity == "Сочи")
    {
        return("sochi");
    }
    if (rusCity == "Казань")
    {
        return("kazan");
    }
    if (rusCity == "Новосибирск")
    {
        return("novosibirsk");
    }
    if (rusCity == "Нижний Новгород")
    {
        return("nizhnynovgorod");
    }
    if (rusCity == "Железногорск")
    {
        return("zheleznogorsk");
    }
    if (rusCity == "Грозный")
    {
        return("grozny");
    }
    if (rusCity == "Краснодар")
    {
        return("krasnodar");
    }
    if (rusCity == "Калининград")
    {
        return("kaliningrad");
    }
    if (rusCity == "Красногорск")
    {
        return("krasnogorsk");
    }
    if (rusCity == "Тюмень")
    {
        return("tyumen");
    }
    if (rusCity == "Белгород")
    {
        return("belgorod");
    }
    if (rusCity == "Ставрополь")
    {
        return("stavropol");
    }
    if (rusCity == "Воронеж")
    {
        return("voronezh");
    }
    if (rusCity == "Курск")
    {
        return("kursk");
    }
    if (rusCity == "Ростов-на-Дону")
    {
        return("rostov-on-Don");
    }
}
