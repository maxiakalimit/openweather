#ifndef CITY_H
#define CITY_H

#include <mainwindow.h>

class City
{
public:
    City();
    City(QString name);

    // название города и название города на русском
    QString name;
    QString russian_name;

    // массив дневной, максимальной и минимальной температуры на три дня в порядке возрастания дат
    QVector<QString> day_temp;
    QVector<QString> temp_max;
    QVector<QString> temp_min;

    // симвлов и описание
    QVector<QString> weather_description;

    // то что пришло от API приведенное в форматы QT
    QVariantMap parsed_data;

    void printNameInDebug();
    void printWeatherDataInDebug();

};

#endif // CITY_H
