#-------------------------------------------------
#
# Project created by QtCreator 2015-09-25T18:42:59
#
#-------------------------------------------------

QT       += core xml
QT       += network
QT       += sql
QT       -= gui

DEFINES  += QT_NO_SSL

win32:RC_ICONS += ico_weather2.ico

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = openWeather
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    city.cpp \
    database.cpp \
    parser.cpp

HEADERS  += mainwindow.h \
    city.h \
    database.h \
    parser.h

FORMS    += mainwindow.ui

RESOURCES += \
    assets.qrc

