#ifndef DATABASE_H
#define DATABASE_H

#include "mainwindow.h"



class Database
{
public:
    Database();
    QVector<QString> getCitiesNameList();
    QString getCityRussianName(QString city_name);
private:
    QSqlDatabase myDB;
};

#endif // DATABASE_H
