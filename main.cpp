#include "mainwindow.h"



//Preloader
void preloader(QSplashScreen* psplash){
    QTime time;
    time.start();
    for (int i = 0; i<100;){
        if (time.elapsed() > 20){
            time.start();
            ++i;
        }
        psplash->showMessage("Загрузка:" +QString::number(i) + "%",
                             Qt::AlignHCenter | Qt::AlignBottom,
                             Qt::white);
        psplash->setStyleSheet("font-size:20pt; text-transform:uppercase");
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QSplashScreen splash(QPixmap(":/assets/img/logo2.png"));

    splash.show();

    preloader(&splash);

    splash.finish(&splash);
    MainWindow w;

    w.show();

    return a.exec();
}
