#include "parser.h"



Parser::Parser()
{
  qDebug() << "parser obj created";
}

//City Parser::getCityObject(QString city_name)
//{
//  return City::City();
//}

QJsonDocument Parser::getJsonWeatherData(QString city_name)
{
  QEventLoop eventLoop;
  QUrl url = "http://api.openweathermap.org/data/2.5/forecast/daily?q=" + city_name + "&mode=json&units=metric&cnt=7&APPID=ce263317c3bdb57a034cffbbde28b20f";
  QNetworkAccessManager mgr;
  QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
  QNetworkRequest req( url );
  QNetworkReply *reply = mgr.get(req);
  eventLoop.exec();

  QJsonDocument result;

  if (reply->error() == QNetworkReply::NoError) {
    result = QJsonDocument::fromJson(reply->readAll());
    QVariantMap test = result.object().toVariantMap();
  }
  else {
    qDebug() << "cant get json data for " + city_name + "reason: " + reply->errorString();
  }
  return result;

}
