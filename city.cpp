#include "city.h"

City::City() {}

City::City(QString city_name)
{
    Database d = Database();
    Parser p = Parser();

    name = city_name;
    russian_name = d.getCityRussianName(name);

    // преобразовываем полученный JSON в QVariant
    parsed_data = p.getJsonWeatherData(name).object().toVariantMap();

    QListIterator<QVariant> i(parsed_data["list"].toList());

    while ( i.hasNext() ) {

        QVariant current_object;
        current_object = i.next();

        // Выдергиваем нужные нам значения из спарсенных данных попутно преобразовывая QVariant в подходящие нам типы
        weather_description.push_back( current_object.toMap()["weather"].toList()[0].toMap()["description"].toString() );
        temp_max.push_back( current_object.toMap()["temp"].toMap()["max"].toString() );
        temp_min.push_back( current_object.toMap()["temp"].toMap()["min"].toString() );
        day_temp.push_back( current_object.toMap()["temp"].toMap()["day"].toString() );

    }

}

void City::printNameInDebug()
{
  qDebug() << this->name;
}

void City::printWeatherDataInDebug()
{
  qDebug() << this->parsed_data;
}
