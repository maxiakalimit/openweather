#include "database.h"

Database::Database()
{

  QString pathToDB = QDir::currentPath()+QString("/weather_db.sqlite");
  QFile dbfile(pathToDB);

  myDB = QSqlDatabase::addDatabase("QSQLITE");
  myDB.setDatabaseName(pathToDB);
  myDB.open();

  QSqlQuery dbtest;

  if ( dbfile.exists() && dbtest.exec("SELECT * FROM cities")  ) {
    qDebug() << "db and table cities exists";
  }
  else {
    qDebug() << "db not exists";
    // создаем файл базы данных
    dbfile.open(QIODevice::WriteOnly);
    dbfile.close();
    myDB.open();
    // sql запрос на построение таблицы
    QSqlQuery createTableQuery;

    createTableQuery.exec("create table cities "
                          "(id integer primary key AUTOINCREMENT, "
                          "name TEXT NOT NULL,"
                          "russian_name TEXT NOT NULL, "
                          "open_weather_city_id INTEGER DEFAULT 0, "
                          "serialized_weather_data TEXT)");

    // sql запрос на добавление списка городов
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Moscow\", \"Москва\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Grozny\", \"Грозный\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Krasnodar\", \"Краснодар\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Kaliningrad\", \"Калининград\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Krasnogorsk\", \"Красногорск\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Tyumen\", \"Тюмень\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Belgorod\", \"Белгород\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Stavropol\", \"Ставрополь\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Voronezh\", \"Воронеж\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Ufa\", \"Уфа\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"SaintPetersburg\", \"Санкт-Петербург\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Chita\", \"Чита\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Chelyabinsk\", \"Челябинск\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Sochi\", \"Сочи\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Kazan\", \"Казань\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Novosibirsk\", \"Новосибирск\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"NizhnyNovgorod\", \"Нижний Новгород\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Zheleznogorsk\", \"Железногорск\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Kursk\", \"Курск\")");
    createTableQuery.exec("INSERT INTO cities (name, russian_name) VALUES (\"Rostov-on-Don\", \"Ростов-на-Дону\")");


    createTableQuery.clear();
  }

  dbtest.clear();
  myDB.close();
}

QVector<QString> Database::getCitiesNameList()
{
  myDB.open();
  QVector<QString> result;
  QSqlQuery getnames;
  if (  getnames.exec("SELECT name FROM cities") )
  {
    while(getnames.next())
    {
      QString name = getnames.value(0).toString();
      result.push_back(name);
    }
    qDebug() << result;
  }
  else
  {
    qDebug() << "cant get cities names";
    qDebug() << myDB.lastError();
  }

  getnames.clear();
  myDB.close();
  return result;
}

QString Database::getCityRussianName(QString city_name)
{
  myDB.open();

  city_name = city_name.toLower();

  QString result;
  QSqlQuery get_russian_name;

  get_russian_name.prepare("SELECT russian_name FROM cities WHERE lower(name) = :city_name");
  get_russian_name.bindValue(":city_name", city_name);
  get_russian_name.exec();

  if ( get_russian_name.next() ) {
    result = get_russian_name.value(0).toString();
  } else {
    qDebug() << get_russian_name.lastError();
  }

  myDB.close();
  get_russian_name.clear();

  return result;
}
